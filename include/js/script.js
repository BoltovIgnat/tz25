$(document).ready(function() {

    $('#contact_form').submit(function(e) {

        var msg   = $('#contact_form').serialize();
        //отмена действия по умолчанию для кнопки submit
        $.ajax({
            type: 'POST',
            url: '/include/js/ajax/ajax.php',
            data: msg,
            success: function(data) {
                $('#results').html(data);
                alert('success');
            },
            error:  function(xhr, str){
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        });
    });
});