<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тест");
?>
<?php


echo "<pre>";
print_r($_POST);
echo "</pre>";
if (!CModule::IncludeModule("iblock"))
{
    ShowMessage(GetMessage("IBLOCK_ERROR"));
    return false;
}
$el = new CIBlockElement;
$arLoadProductArray = Array(
    'MODIFIED_BY' => $GLOBALS['USER']->GetID(), // элемент изменен текущим пользователем
    'IBLOCK_SECTION_ID' => 16, // элемент лежит в корне раздела
    'IBLOCK_ID' => 2,
    'PROPERTY_VALUES' => $_POST,
    'NAME' => 'Элемент',
    'CODE' => 'ss',
    'ACTIVE' => 'N', // активен
    'PREVIEW_TEXT' => 'текст для списка элементов',
    'DETAIL_TEXT' => 'текст для детального просмотра',
    'DETAIL_PICTURE' => $_FILES['MORE_PHOTO'] // картинка, загружаемая из файлового поля веб-формы с именем DETAIL_PICTURE
);

if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
    echo 'New ID: '.$PRODUCT_ID;
} else {
    echo 'Error: '.$el->LAST_ERROR;
}
