<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$type = "text";
$required = "";
?>
<p id="results"></p>
<div class="container">

    <form class="well form-horizontal" action="/include/js/ajax/ajax.php" method="post"  id="contact_form">
        <fieldset>

            <!-- Form Name -->
            <legend>Заполнение элемента инфоблока</legend>

            <!-- Text input-->
            <? foreach ($arResult['PROPS'] as $pid => $arProperty): ?>
                <? if ($arProperty['TYPE'] == 'F'):
                    $type = 'file';
                elseif ($arProperty['TYPE'] == 'N'):
                    $type = 'number';
                elseif ($arProperty['TYPE'] == 'S'):
                    $type = 'text';
                elseif ($arProperty['TYPE'] == 'E'):
                    $type = 'text';
                elseif ($arProperty['TYPE'] == 'L'):
                    $type = 'checkbox';
                endif;
                if ($arProperty['IS_REQUIRED'] == 'Y'):
                    $required = '$required';
                else:
                    $required = '';
                endif; ?>
                <div class="form-group">
                    <label class="col-md-4 control-label"><?=$arProperty['NAME']?></label>
                    <div class="col-md-4 inputGroupContainer">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input  name="<?=$pid?>" placeholder="<?=$pid?>" <?=$required?> class="form-control"  type="<?=$type?>">
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
            
            <!-- Success message -->
            <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-warning" >Send <span class="glyphicon glyphicon-send"></span></button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
</div><!-- /.container -->
