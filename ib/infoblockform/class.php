<?

class CDemoSqr extends CBitrixComponent
{
    public function GetProps()
    {
        $db_props = CIBlockElement::GetProperty($this->arParams[IBLOCK_ID], array(), "sort", "asc", array());
        $PROPS = array();
        while($ar_props = $db_props->Fetch()){
            $PROPS[$ar_props['CODE']]['NAME'] = $ar_props['NAME'];
            $PROPS[$ar_props['CODE']]['VALUE'] = $ar_props['VALUE'];
            $PROPS[$ar_props['CODE']]['TYPE'] = $ar_props['PROPERTY_TYPE'];
            $PROPS[$ar_props['CODE']]['IS_REQUIRED'] = $ar_props['IS_REQUIRED'];
            //$PROPS['Temp'][] = $ar_props;
        }

        return $PROPS;
    }
    
    public function executeComponent()
    {
        $this->arResult["PROPS"] = $this->GetProps();
        $this->includeComponentTemplate();
    }
};